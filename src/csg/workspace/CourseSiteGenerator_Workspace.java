/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;


import csg.CourseSiteGeneratorApp;
import static csg.CourseSiteGeneratorPropertyType.*;
import csg.data.ClassCourse_Prototype;
import csg.data.CourseSiteGenerator_Data;
import csg.data.MeetingTime;
import csg.data.ScheduleItem;
import csg.data.TeachingAssistant_Prototype;
import csg.data.TimeSlot;
import csg.transactions.Add_Remove_Meeting_Transaction;
import csg.transactions.Add_Remove_Schedule_Transaction;
import csg.transactions.Clear_Schedule_Transaction;
import csg.transactions.Edit_Meeting_Transaction;
import csg.transactions.Edit_Schedule_Transaction;
import csg.transactions.RemoveTA_Transaction;
import csg.transactions.Edit_Site_Transaction;
import csg.transactions.Edit_Syllabus_Transaction;
import csg.workspace.controllers.CourseSiteGenerator_Controller;
import csg.workspace.foolproof.CourseSiteGenerator_FoolproofDesign;
import static csg.workspace.style.CSGStyle.*;
import static djf.AppPropertyType.EXPORT_BUTTON;
import static djf.AppPropertyType.SAVE_BUTTON;
import djf.components.AppWorkspaceComponent;
import djf.modules.AppFoolproofModule;
import djf.modules.AppGUIModule;
import static djf.modules.AppGUIModule.ENABLED;
import djf.ui.AppNodesBuilder;
import java.awt.image.BufferedImage;
import javafx.scene.image.Image;
import java.io.File;
import java.io.IOException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import properties_manager.PropertiesManager;
/**
 *
 * @author bflor
 */

public class CourseSiteGenerator_Workspace extends AppWorkspaceComponent {
    
    Stage editTAWindow = new Stage();
    final FileChooser fileChooser = new FileChooser();
    public String favicon ="";
    public String navbar = "";
    public String leftFooter = "";
    public String rightFooter = "";
    
    ObservableList<String> subject = FXCollections.observableArrayList(
            "Please select a Subject",
            "AMS",
            "ARS",
            "CHE",
            "CSE",
            "ISE",
            "PHY",
            "PSY",
            "THR"
    );
    
    ObservableList<String> semesters = FXCollections.observableArrayList(
            "Please select a Semester",
            "Fall",
            "Winter",
            "Spring",
            "Summer"
    ); 
    ObservableList<String> scheduleItemTypes = FXCollections.observableArrayList(
            "Lecture",
            "HW",
            "Recitation",
            "Holiday",
            "Reference"
    ); 
    
    ObservableList<String> years = generateList(2018, 50);
    
    ObservableList<String> number = generateList(100, 400);
    
    ObservableList<String> css = FXCollections.observableArrayList();
    ObservableList<String> startTime = FXCollections.observableArrayList();
    ObservableList<String> endTime = FXCollections.observableArrayList();
            
    private ObservableList<String> generateList(int startNum, int amount) {
        int num = startNum;
        ObservableList<String> list = FXCollections.observableArrayList();
        list.add("Please select a Semester");
        for (int i = 0; i < amount; i++) {
            list.add(""+num);
            num++;
        }
        return list;
    }
    
    
    public CourseSiteGenerator_Workspace(CourseSiteGeneratorApp app) {
        super(app);
        
        loadCSSFiles();
        //LAYOUT THE APP
        initLayout();
        
        //Initialize the EVENT HANDLERS
        initControllers();
        
        //Initialize FOOLPROOF DESIGN
        initFoolproofDesign();
        
        updateDir(); //init the export directory
    }
    
    private void initLayout() {
        //load the font families for the combo box
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        

        //build all JavaFX components (nodes)
        AppNodesBuilder csgBuilder = app.getGUIModule().getNodesBuilder();
        AppGUIModule gui = app.getGUIModule();
        
        //NEW WINDOW PANE FOR EDITING
        VBox editTA = csgBuilder.buildVBox(CSG_EDIT_VBOX, null, CLASS_CSG_EDIT_BOX, ENABLED);
        editTA.setSpacing(25);
        csgBuilder.buildLabel(CSG_EDIT_LABEL, editTA, CLASS_CSG_BIGLABEL, ENABLED);
        
        HBox nameField = csgBuilder.buildHBox(CSG_EDIT_HBOX_NAME, editTA, CLASS_CSG_BOX, ENABLED);
        nameField.setSpacing(46);
        csgBuilder.buildLabel(CSG_EDIT_LABEL_NAME, nameField, CLASS_CSG_LABEL, ENABLED);
        csgBuilder.buildTextField(CSG_EDIT_FIELD_NAME, nameField, CLASS_CSG_FIELD, ENABLED).setPrefWidth(350);
        
        HBox emailField = csgBuilder.buildHBox(CSG_EDIT_HBOX_EMAIL, editTA, CLASS_CSG_BOX, ENABLED);
        emailField.setSpacing(50);
        csgBuilder.buildLabel(CSG_EDIT_LABEL_EMAIL, emailField, CLASS_CSG_LABEL, ENABLED);
        csgBuilder.buildTextField(CSG_EDIT_FIELD_EMAIL, emailField, CLASS_CSG_FIELD, ENABLED).setPrefWidth(350);
        
        Scene editTAScene = new Scene(editTA, 500, 300);
        editTAWindow.setScene(editTAScene);
        
        
        //TAB PANES
        //TODO: Change text to language properties. Lines with LANG EDIT comments
        HBox main = new HBox();
        TabPane tabPane = csgBuilder.buildTabPane(CSG_TABPANE, CLASS_CSG_TABPANE, ENABLED);
        tabPane.prefWidthProperty().bind(gui.getWindow().widthProperty().multiply(.95));
        tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
        Tab siteTab = new Tab();
        siteTab.setText("Site");   //LANG EDIT
        Tab sylTab = new Tab();
        sylTab.setText("Syllabus");    //LANG EDIT
        Tab meetTab = new Tab();
        meetTab.setText("Meeting Times");  //LANG EDIT
        Tab ohTab = new Tab();
        ohTab.setText("Office Hours");     //LANG EDIT
        Tab schedTab = new Tab();
        schedTab.setText("Schedule");      //LANG EDIT
        
        //SITE TAB
        ScrollPane site = new ScrollPane();
        VBox siteBox = csgBuilder.buildVBox(CSG_SITE_VBOX, null, CLASS_CSG_BACKGROUND, ENABLED);
        siteBox.prefWidthProperty().bind(tabPane.widthProperty().multiply(.99));
        siteBox.prefHeightProperty().bind(tabPane.heightProperty().multiply(1));
        
        //SITE TAB: BANNER BOX
        VBox banner = csgBuilder.buildVBox(CSG_SITE_VBOX_BANNER, siteBox, CLASS_CSG_BOX, ENABLED);
        banner.prefWidthProperty().bind(site.widthProperty().multiply(.5));
        csgBuilder.buildLabel(CSG_SITE_LABEL_BANNER, banner, CLASS_CSG_BIGLABEL, ENABLED);
        
        HBox subNum = new HBox();
        banner.getChildren().add(subNum);
        csgBuilder.buildLabel(CSG_SITE_BANNER_LABEL_SUBJECT, subNum, CLASS_CSG_LABEL, ENABLED);
        (csgBuilder.buildComboBox(CSG_SITE_BANNER_COMBOBOX_SUBJECT, subject, "", subNum, CLASS_CSG_COMBOBOX, ENABLED)).setEditable(true);
        csgBuilder.buildLabel(CSG_SITE_BANNER_LABEL_NUMBER, subNum, CLASS_CSG_LABEL, ENABLED);
        csgBuilder.buildComboBox(CSG_SITE_BANNER_COMBOBOX_NUMBER, number, "", subNum, CLASS_CSG_COMBOBOX, ENABLED).setEditable(true);
        
        HBox semYear = new HBox();
        banner.getChildren().add(semYear);
        csgBuilder.buildLabel(CSG_SITE_BANNER_LABEL_SEMESTER, semYear, CLASS_CSG_LABEL, ENABLED);
        csgBuilder.buildComboBox(CSG_SITE_BANNER_COMBOBOX_SEMESTER, semesters, "", semYear, CLASS_CSG_COMBOBOX, ENABLED).setEditable(true);
        csgBuilder.buildLabel(CSG_SITE_BANNER_LABEL_YEAR, semYear, CLASS_CSG_LABEL, ENABLED);
        csgBuilder.buildComboBox(CSG_SITE_BANNER_COMBOBOX_YEAR, years, "", semYear, CLASS_CSG_COMBOBOX, ENABLED).setEditable(true);
        
        HBox tit = new HBox();
        banner.getChildren().add(tit);
        csgBuilder.buildLabel(CSG_SITE_BANNER_LABEL_TITLE, tit, CLASS_CSG_LABEL, ENABLED);
        csgBuilder.buildTextField(CSG_SITE_BANNER_TEXTFIELD_TITLE, tit, CLASS_CSG_FIELD, ENABLED);
        
        HBox dir = new HBox();
        banner.getChildren().add(dir);
        csgBuilder.buildLabel(CSG_SITE_BANNER_LABEL_DIR, dir, CLASS_CSG_LABEL, ENABLED);
        csgBuilder.buildLabel(CSG_SITE_BANNER_LABEL_DIR_EDIT, dir, CLASS_CSG_SUBLABEL, ENABLED);
        csgBuilder.buildLabel(CSG_SITE_BANNER_LABEL_DIR_ERROR, dir, CLASS_CSG_SUBLABEL, ENABLED).setVisible(false); //used to change to error text.
  
        
        //SITE TAB: PAGES BOX
        HBox pages = csgBuilder.buildHBox(CSG_SITE_HBOX_PAGES, siteBox, CLASS_CSG_BOX, ENABLED);
        csgBuilder.buildLabel(CSG_SITE_LABEL_PAGES, pages, CLASS_CSG_BIGLABEL, ENABLED);
        csgBuilder.buildCheckBox(CSG_SITE_PAGES_CHECK_HOME, pages, CLASS_CSG_CHECK, ENABLED);
        csgBuilder.buildCheckBox(CSG_SITE_PAGES_CHECK_SYL, pages, CLASS_CSG_CHECK, ENABLED);
        csgBuilder.buildCheckBox(CSG_SITE_PAGES_CHECK_SCHED, pages, CLASS_CSG_CHECK, ENABLED);
        csgBuilder.buildCheckBox(CSG_SITE_PAGES_CHECK_HW, pages, CLASS_CSG_CHECK, ENABLED);

        //SITE TAB: STYLE BOX
        VBox style = csgBuilder.buildVBox(CSG_SITE_VBOX_STYLE, siteBox, CLASS_CSG_BOX, ENABLED);
        csgBuilder.buildLabel(CSG_SITE_LABEL_STYLE, style, CLASS_CSG_BIGLABEL, ENABLED);
        
        HBox fav = new HBox();
        style.getChildren().add(fav);
        csgBuilder.buildTextButton(CSG_SITE_STYLE_BUTTON_FAV, fav, CLASS_CSG_BUTTON, ENABLED);
        csgBuilder.buildIconButton(CSG_SITE_STYLE_ICON_FAV, fav, CLASS_CSG_IMAGE, ENABLED);
        
        HBox nav = new HBox();
        style.getChildren().add(nav);
        csgBuilder.buildTextButton(CSG_SITE_STYLE_BUTTON_NAV, nav, CLASS_CSG_BUTTON, ENABLED);
        csgBuilder.buildIconButton(CSG_SITE_STYLE_ICON_NAV, nav, CLASS_CSG_IMAGE, ENABLED);
        
        HBox left = new HBox();
        style.getChildren().add(left);
        csgBuilder.buildTextButton(CSG_SITE_STYLE_BUTTON_LEFT, left, CLASS_CSG_BUTTON, ENABLED);
        csgBuilder.buildIconButton(CSG_SITE_STYLE_ICON_LEFT, left, CLASS_CSG_IMAGE, ENABLED);
        
        HBox right = new HBox();
        style.getChildren().add(right);
        csgBuilder.buildTextButton(CSG_SITE_STYLE_BUTTON_RIGHT, right, CLASS_CSG_BUTTON, ENABLED);
        csgBuilder.buildIconButton(CSG_SITE_STYLE_ICON_RIGHT, right, CLASS_CSG_IMAGE, ENABLED);
        
        HBox cssBox = new HBox();
        style.getChildren().add(cssBox);
        csgBuilder.buildLabel(CSG_SITE_STYLE_LABEL_CSS, cssBox, CLASS_CSG_LABEL, ENABLED);
                csgBuilder.buildComboBox(CSG_SITE_STYLE_COMBOBOX_CSS, css, "", cssBox, CLASS_CSG_COMBOBOX, ENABLED);
        //COMBOBOX
        
        HBox note = new HBox();
        style.getChildren().add(note);
        csgBuilder.buildLabel(CSG_SITE_STYLE_LABEL_NOTE, note, CLASS_CSG_LABEL, ENABLED);
        
        //SITE TAB: INSTRUCTOR BOX
        VBox instruc = csgBuilder.buildVBox(CSG_SITE_VBOX_INSTRUCTOR, siteBox, CLASS_CSG_BOX, ENABLED);
        csgBuilder.buildLabel(CSG_SITE_LABEL_INSTRUCTOR, instruc, CLASS_CSG_BIGLABEL, ENABLED);
        
        HBox nameRoom = new HBox();
        instruc.getChildren().add(nameRoom);
        csgBuilder.buildLabel(CSG_SITE_INSTRUCTOR_LABEL_NAME, nameRoom, CLASS_CSG_LABEL, ENABLED);
        csgBuilder.buildTextField(CSG_SITE_INSTRUCTOR_FIELD_NAME, nameRoom, CLASS_CSG_FIELD, ENABLED);
        csgBuilder.buildLabel(CSG_SITE_INSTRUCTOR_LABEL_ROOM, nameRoom, CLASS_CSG_LABEL, ENABLED);
        csgBuilder.buildTextField(CSG_SITE_INSTRUCTOR_FIELD_ROOM, nameRoom, CLASS_CSG_FIELD, ENABLED);
        
        HBox emailHome = new HBox();
        instruc.getChildren().add(emailHome);
        csgBuilder.buildLabel(CSG_SITE_INSTRUCTOR_LABEL_EMAIL, emailHome, CLASS_CSG_LABEL, ENABLED);
        csgBuilder.buildTextField(CSG_SITE_INSTRUCTOR_FIELD_EMAIL, emailHome, CLASS_CSG_FIELD, ENABLED);
        csgBuilder.buildLabel(CSG_SITE_INSTRUCTOR_LABEL_HOME, emailHome, CLASS_CSG_LABEL, ENABLED);
        csgBuilder.buildTextField(CSG_SITE_INSTRUCTOR_FIELD_HOME, emailHome, CLASS_CSG_FIELD, ENABLED);
        
        HBox officeHours = new HBox();
        instruc.getChildren().add(officeHours);
        csgBuilder.buildIconButton(CSG_SITE_INSTRUCTOR_EXPAND_OH, officeHours, CLASS_CSG_MINIMIZE, ENABLED);
        csgBuilder.buildLabel(CSG_SITE_INSTRUCTOR_LABEL_OH, officeHours, CLASS_CSG_LABEL, ENABLED);
        csgBuilder.buildTextArea(CSG_SITE_INSTRUCTOR_TEXTAREA_OH, instruc, CLASS_CSG_AREA, ENABLED);
        
        //SITE TAB: ADD CONTENT
        site.setContent(siteBox);
        siteTab.setContent(site);
        tabPane.getTabs().add(siteTab);
        
        //SYLLABUS TAB
        ScrollPane syl = new ScrollPane();
        VBox sylBox = csgBuilder.buildVBox(CSG_SYL_VBOX, null, CLASS_CSG_BACKGROUND, ENABLED);
        sylBox.prefWidthProperty().bind(tabPane.widthProperty().multiply(.99));
        sylBox.prefHeightProperty().bind(tabPane.heightProperty().multiply(1));
        
        VBox desc = csgBuilder.buildVBox(CSG_SYL_VBOX_DESCRIPTION, sylBox, CLASS_CSG_BOX, ENABLED);
        HBox descHold = new HBox();
        desc.getChildren().add(descHold);
        csgBuilder.buildIconButton(CSG_SYL_EXPAND_DESCRIPTION, descHold, CLASS_CSG_MINIMIZE, ENABLED);
        csgBuilder.buildLabel(CSG_SYL_LABEL_DESCRIPTION, descHold, CLASS_CSG_BIGLABEL, ENABLED);
        csgBuilder.buildTextArea(CSG_SYL_TEXTAREA_DESCRIPTION, desc, CLASS_CSG_AREA, ENABLED);
        
        VBox topics = csgBuilder.buildVBox(CSG_SYL_VBOX_TOPICS, sylBox, CLASS_CSG_BOX, ENABLED);
        HBox topicsHold = new HBox();
        topics.getChildren().add(topicsHold);
        csgBuilder.buildIconButton(CSG_SYL_EXPAND_TOPICS, topicsHold, CLASS_CSG_MINIMIZE, ENABLED);
        csgBuilder.buildLabel(CSG_SYL_LABEL_TOPICS, topicsHold, CLASS_CSG_BIGLABEL, ENABLED);
        csgBuilder.buildTextArea(CSG_SYL_TEXTAREA_TOPICS, topics, CLASS_CSG_AREA, ENABLED);
        
        VBox prereq = csgBuilder.buildVBox(CSG_SYL_VBOX_PREREQ, sylBox, CLASS_CSG_BOX, ENABLED);
        HBox preHold = new HBox();
        prereq.getChildren().add(preHold);
        csgBuilder.buildIconButton(CSG_SYL_EXPAND_PREREQ, preHold, CLASS_CSG_MINIMIZE, ENABLED);
        csgBuilder.buildLabel(CSG_SYL_LABEL_PREREQ, preHold, CLASS_CSG_BIGLABEL, ENABLED);
        csgBuilder.buildTextArea(CSG_SYL_TEXTAREA_PREREQ, prereq, CLASS_CSG_AREA, ENABLED);
        
        VBox outcomes = csgBuilder.buildVBox(CSG_SYL_VBOX_OUTCOMES, sylBox, CLASS_CSG_BOX, ENABLED);
        HBox outHold = new HBox();
        outcomes.getChildren().add(outHold);
        csgBuilder.buildIconButton(CSG_SYL_EXPAND_OUTCOMES, outHold, CLASS_CSG_MINIMIZE, ENABLED);
        csgBuilder.buildLabel(CSG_SYL_LABEL_OUTCOMES, outHold, CLASS_CSG_BIGLABEL, ENABLED);
        csgBuilder.buildTextArea(CSG_SYL_TEXTAREA_OUTCOMES, outcomes, CLASS_CSG_AREA, ENABLED);
        
        VBox textbooks = csgBuilder.buildVBox(CSG_SYL_VBOX_TEXTBOOKS, sylBox, CLASS_CSG_BOX, ENABLED);
        HBox textHold = new HBox();
        textbooks.getChildren().add(textHold);
        csgBuilder.buildIconButton(CSG_SYL_EXPAND_TEXTBOOKS, textHold, CLASS_CSG_MINIMIZE, ENABLED);
        csgBuilder.buildLabel(CSG_SYL_LABEL_TEXTBOOKS, textHold, CLASS_CSG_BIGLABEL, ENABLED);
        csgBuilder.buildTextArea(CSG_SYL_TEXTAREA_TEXTBOOKS, textbooks, CLASS_CSG_AREA, ENABLED);
        
        VBox graded = csgBuilder.buildVBox(CSG_SYL_VBOX_GRADED, sylBox, CLASS_CSG_BOX, ENABLED);
        HBox gradedHold = new HBox();
        graded.getChildren().add(gradedHold);
        csgBuilder.buildIconButton(CSG_SYL_EXPAND_GRADED, gradedHold, CLASS_CSG_MINIMIZE, ENABLED);
        csgBuilder.buildLabel(CSG_SYL_LABEL_GRADED, gradedHold, CLASS_CSG_BIGLABEL, ENABLED);
        csgBuilder.buildTextArea(CSG_SYL_TEXTAREA_GRADED, graded, CLASS_CSG_AREA, ENABLED);
        
        VBox gradeNote = csgBuilder.buildVBox(CSG_SYL_VBOX_NOTE, sylBox, CLASS_CSG_BOX, ENABLED);
        HBox gradeNoteHold = new HBox();
        gradeNote.getChildren().add(gradeNoteHold);
        csgBuilder.buildIconButton(CSG_SYL_EXPAND_NOTE, gradeNoteHold, CLASS_CSG_MINIMIZE, ENABLED);
        csgBuilder.buildLabel(CSG_SYL_LABEL_NOTE, gradeNoteHold, CLASS_CSG_BIGLABEL, ENABLED);
        csgBuilder.buildTextArea(CSG_SYL_TEXTAREA_NOTE, gradeNote, CLASS_CSG_AREA, ENABLED);
        
        VBox dishonesty = csgBuilder.buildVBox(CSG_SYL_VBOX_DISHONESTY, sylBox, CLASS_CSG_BOX, ENABLED);
        HBox dishonestHold = new HBox();
        dishonesty.getChildren().add(dishonestHold);
        csgBuilder.buildIconButton(CSG_SYL_EXPAND_DISHONESTY, dishonestHold, CLASS_CSG_MINIMIZE, ENABLED);
        csgBuilder.buildLabel(CSG_SYL_LABEL_DISHONESTY, dishonestHold, CLASS_CSG_BIGLABEL, ENABLED);
        csgBuilder.buildTextArea(CSG_SYL_TEXTAREA_DISHONESTY, dishonesty, CLASS_CSG_AREA, ENABLED);

        VBox assist = csgBuilder.buildVBox(CSG_SYL_VBOX_ASSIST, sylBox, CLASS_CSG_BOX, ENABLED);
        HBox assistHold = new HBox();
        assist.getChildren().add(assistHold);
        csgBuilder.buildIconButton(CSG_SYL_EXPAND_ASSIST, assistHold, CLASS_CSG_MINIMIZE, ENABLED);
        csgBuilder.buildLabel(CSG_SYL_LABEL_ASSIST, assistHold, CLASS_CSG_BIGLABEL, ENABLED);
        csgBuilder.buildTextArea(CSG_SYL_TEXTAREA_ASSIST, assist, CLASS_CSG_AREA, ENABLED);     
        
        //SYLLABUS TAB: ADD CONTENT
        syl.setContent(sylBox);
        sylTab.setContent(syl);
        tabPane.getTabs().add(sylTab);
        
        //MEETING TIMES TAB
        ScrollPane meet = new ScrollPane();
        VBox meetBox = csgBuilder.buildVBox(CSG_SYL_VBOX, null, CLASS_CSG_BACKGROUND, ENABLED);
        meetBox.prefWidthProperty().bind(tabPane.widthProperty().multiply(.99));
        meetBox.prefHeightProperty().bind(tabPane.heightProperty().multiply(1));
        
        VBox lect = csgBuilder.buildVBox(CSG_MEET_VBOX_LECTURES, meetBox, CLASS_CSG_BOX, ENABLED);
        HBox lectHold = new HBox();
        lect.getChildren().add(lectHold);
        csgBuilder.buildIconButton(CSG_MEET_ADD_LECTURES, lectHold, CLASS_CSG_ADD, ENABLED);
        csgBuilder.buildIconButton(CSG_MEET_REMOVE_LECTURES, lectHold, CLASS_CSG_REMOVE, ENABLED);
        csgBuilder.buildLabel(CSG_MEET_LABEL_LECTURES, lectHold, CLASS_CSG_BIGLABEL, ENABLED);
        
        TableView<ClassCourse_Prototype> lecTable = csgBuilder.buildTableView(CSG_MEET_TABLEVIEW_LECTURES, lect, CLASS_CSG_TABLEVIEW, ENABLED);
        lecTable.setEditable(true);
        lecTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        lecTable.getSelectionModel().setCellSelectionEnabled(true);
        TableColumn lecSec = csgBuilder.buildTableColumn(CSG_MEET_LECTURES_COLUMN_SECTION, lecTable, CLASS_CSG_COLUMN);
        lecSec.setCellValueFactory(new PropertyValueFactory<String, String>("section"));
        lecSec.setCellFactory(TextFieldTableCell.forTableColumn());
        lecSec.prefWidthProperty().bind(lecTable.widthProperty().multiply(1.0/4.0));
        TableColumn lecDay = csgBuilder.buildTableColumn(CSG_MEET_LECTURES_COLUMN_DAYS, lecTable, CLASS_CSG_COLUMN);
        lecDay.setCellValueFactory(new PropertyValueFactory<String, String>("days"));
        lecDay.setCellFactory(TextFieldTableCell.forTableColumn());
        lecDay.prefWidthProperty().bind(lecTable.widthProperty().multiply(1.0/4.0));
        TableColumn lecTim = csgBuilder.buildTableColumn(CSG_MEET_LECTURES_COLUMN_TIME, lecTable, CLASS_CSG_COLUMN);
        lecTim.setCellValueFactory(new PropertyValueFactory<String, String>("time"));
        lecTim.setCellFactory(TextFieldTableCell.forTableColumn());
        lecTim.prefWidthProperty().bind(lecTable.widthProperty().multiply(1.0/4.0));
        TableColumn lecRom = csgBuilder.buildTableColumn(CSG_MEET_LECTURES_COLUMN_ROOM, lecTable, CLASS_CSG_COLUMN);
        lecRom.setCellValueFactory(new PropertyValueFactory<String, String>("room"));
        lecRom.setCellFactory(TextFieldTableCell.forTableColumn());
        lecRom.prefWidthProperty().bind(lecTable.widthProperty().multiply(1.0/4.0));
        
        VBox rec = csgBuilder.buildVBox(CSG_MEET_VBOX_RECITATIONS, meetBox, CLASS_CSG_BOX, ENABLED);
        HBox recHold = new HBox();
        rec.getChildren().add(recHold);
        csgBuilder.buildIconButton(CSG_MEET_ADD_RECITATIONS, recHold, CLASS_CSG_ADD, ENABLED);
        csgBuilder.buildIconButton(CSG_MEET_REMOVE_RECITATIONS, recHold, CLASS_CSG_REMOVE, ENABLED);
        csgBuilder.buildLabel(CSG_MEET_LABEL_RECITATIONS, recHold, CLASS_CSG_BIGLABEL, ENABLED);
        
        TableView<ClassCourse_Prototype> recTable = csgBuilder.buildTableView(CSG_MEET_TABLEVIEW_RECITATIONS, rec, CLASS_CSG_TABLEVIEW, ENABLED);
        recTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        recTable.setEditable(true);
        recTable.getSelectionModel().setCellSelectionEnabled(true);
        TableColumn recSec = csgBuilder.buildTableColumn(CSG_MEET_RECITATION_COLUMN_SECTION, recTable, CLASS_CSG_COLUMN);
        recSec.setCellValueFactory(new PropertyValueFactory<String, String>("section"));
        recSec.setCellFactory(TextFieldTableCell.forTableColumn());
        recSec.prefWidthProperty().bind(lecTable.widthProperty().multiply(1.0/8.0));
        TableColumn recDayTime = csgBuilder.buildTableColumn(CSG_MEET_RECITATION_COLUMN_DAYTIME, recTable, CLASS_CSG_COLUMN);
        recDayTime.setCellValueFactory(new PropertyValueFactory<String, String>("dayTime"));
        recDayTime.setCellFactory(TextFieldTableCell.forTableColumn());
        recDayTime.prefWidthProperty().bind(lecTable.widthProperty().multiply(2.0/8.0));
        TableColumn recRom = csgBuilder.buildTableColumn(CSG_MEET_RECITATION_COLUMN_ROOM, recTable, CLASS_CSG_COLUMN);
        recRom.setCellValueFactory(new PropertyValueFactory<String, String>("room"));
        recRom.setCellFactory(TextFieldTableCell.forTableColumn());
        recRom.prefWidthProperty().bind(lecTable.widthProperty().multiply(1.0/8.0));
        TableColumn recTA1 = csgBuilder.buildTableColumn(CSG_MEET_RECITATION_COLUMN_TA1, recTable, CLASS_CSG_COLUMN);
        recTA1.setCellValueFactory(new PropertyValueFactory<String, String>("ta1"));
        recTA1.setCellFactory(TextFieldTableCell.forTableColumn());
        recTA1.prefWidthProperty().bind(lecTable.widthProperty().multiply(2.0/8.0));
        TableColumn recTA2 = csgBuilder.buildTableColumn(CSG_MEET_RECITATION_COLUMN_TA2, recTable, CLASS_CSG_COLUMN);
        recTA2.setCellValueFactory(new PropertyValueFactory<String, String>("ta2"));
        recTA2.setCellFactory(TextFieldTableCell.forTableColumn());
        recTA2.prefWidthProperty().bind(lecTable.widthProperty().multiply(2.0/8.0));
        
        VBox lab = csgBuilder.buildVBox(CSG_MEET_VBOX_LABS, meetBox, CLASS_CSG_BOX, ENABLED);
        HBox labHold = new HBox();
        lab.getChildren().add(labHold);
        csgBuilder.buildIconButton(CSG_MEET_ADD_LABS, labHold, CLASS_CSG_ADD, ENABLED);
        csgBuilder.buildIconButton(CSG_MEET_REMOVE_LABS, labHold, CLASS_CSG_REMOVE, ENABLED);
        csgBuilder.buildLabel(CSG_MEET_LABEL_LABS, labHold, CLASS_CSG_BIGLABEL, ENABLED);
        
        TableView<ClassCourse_Prototype> labTable = csgBuilder.buildTableView(CSG_MEET_TABLEVIEW_LABS, lab, CLASS_CSG_TABLEVIEW, ENABLED);
        labTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        labTable.setEditable(true);
        labTable.getSelectionModel().setCellSelectionEnabled(true);
        TableColumn labSec = csgBuilder.buildTableColumn(CSG_MEET_LABS_COLUMN_SECTION, labTable, CLASS_CSG_COLUMN);
        labSec.setCellValueFactory(new PropertyValueFactory<String, String>("section"));
        labSec.prefWidthProperty().bind(lecTable.widthProperty().multiply(2.0/9.0));
        labSec.setCellFactory(TextFieldTableCell.forTableColumn());
        TableColumn labDayTime = csgBuilder.buildTableColumn(CSG_MEET_LABS_COLUMN_DAYTIME, labTable, CLASS_CSG_COLUMN);
        labDayTime.setCellValueFactory(new PropertyValueFactory<String, String>("dayTime"));
        labDayTime.setCellFactory(TextFieldTableCell.forTableColumn());
        labDayTime.prefWidthProperty().bind(lecTable.widthProperty().multiply(3.0/9.0));
        TableColumn labRom = csgBuilder.buildTableColumn(CSG_MEET_LABS_COLUMN_ROOM, labTable, CLASS_CSG_COLUMN);
        labRom.setCellValueFactory(new PropertyValueFactory<String, String>("room"));
        labRom.setCellFactory(TextFieldTableCell.forTableColumn());
        labRom.prefWidthProperty().bind(lecTable.widthProperty().multiply(2.0/9.0));
        TableColumn labTA1 = csgBuilder.buildTableColumn(CSG_MEET_LABS_COLUMN_TA1, labTable, CLASS_CSG_COLUMN);
        labTA1.setCellValueFactory(new PropertyValueFactory<String, String>("ta1"));
        labTA1.setCellFactory(TextFieldTableCell.forTableColumn());
        labTA1.prefWidthProperty().bind(lecTable.widthProperty().multiply(1.0/9.0));
        TableColumn labTA2 = csgBuilder.buildTableColumn(CSG_MEET_LABS_COLUMN_TA2, labTable, CLASS_CSG_COLUMN);
        labTA2.setCellValueFactory(new PropertyValueFactory<String, String>("ta2"));
        labTA2.setCellFactory(TextFieldTableCell.forTableColumn());
        labTA2.prefWidthProperty().bind(lecTable.widthProperty().multiply(1.0/9.0));
        //MEETING TIMES TAB: ADD CONTENT
        meet.setContent(meetBox);
        meetTab.setContent(meet);
        tabPane.getTabs().add(meetTab);
        
        //OFFICE HOURS TAB
        ScrollPane oh = new ScrollPane();
        VBox ohBox = csgBuilder.buildVBox(CSG_OH_VBOX, null, CLASS_CSG_BACKGROUND, ENABLED);
        ohBox.prefWidthProperty().bind(tabPane.widthProperty().multiply(.99));
        ohBox.prefHeightProperty().bind(tabPane.heightProperty().multiply(1));
        
        VBox ohInfo = csgBuilder.buildVBox(CSG_OH_VBOX, ohBox, CLASS_CSG_BOX, ENABLED);
        HBox taHold = new HBox();
        ohInfo.getChildren().add(taHold);
        ToggleGroup taGroup = new ToggleGroup();
        csgBuilder.buildIconButton(CSG_OH_DELETE_TAS, taHold, CLASS_CSG_MINIMIZE, ENABLED);
        csgBuilder.buildLabel(CSG_OH_LABEL_TAS, taHold, CLASS_CSG_BIGLABEL, ENABLED);
        ((RadioButton) csgBuilder.buildRadioButton(CSG_OH_TA_RADIO_ALL, taHold, CLASS_CSG_RADIO, ENABLED)).setToggleGroup(taGroup);
        ((RadioButton) gui.getGUINode(CSG_OH_TA_RADIO_ALL)).setSelected(true);
        ((RadioButton) csgBuilder.buildRadioButton(CSG_OH_TA_RADIO_GRAD, taHold, CLASS_CSG_RADIO, ENABLED)).setToggleGroup(taGroup);
        ((RadioButton) csgBuilder.buildRadioButton(CSG_OH_TA_RADIO_UNDERGRAD, taHold, CLASS_CSG_RADIO, ENABLED)).setToggleGroup(taGroup);
        
        TableView<TeachingAssistant_Prototype> taTable = csgBuilder.buildTableView(CSG_OH_TABLEVIEW_TA, ohInfo, CLASS_CSG_TABLEVIEW, ENABLED);
        TableColumn taName = csgBuilder.buildTableColumn(CSG_OH_TA_COLUMN_NAME, taTable, CLASS_CSG_COLUMN);
        taName.setCellValueFactory(new PropertyValueFactory<String, String>("name"));
        taName.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0/4.0));
        TableColumn taEmail = csgBuilder.buildTableColumn(CSG_OH_TA_COLUMN_EMAIL, taTable, CLASS_CSG_COLUMN);
        taEmail.setCellValueFactory(new PropertyValueFactory<String, String>("email"));
        taEmail.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0/4.0));
        TableColumn taSlots = csgBuilder.buildTableColumn(CSG_OH_TA_COLUMN_SLOTS, taTable, CLASS_CSG_COLUMN);
        taSlots.setCellValueFactory(new PropertyValueFactory<String, String>("slots"));
        taSlots.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0/4.0));
        TableColumn taType = csgBuilder.buildTableColumn(CSG_OH_TA_COLUMN_TYPE, taTable, CLASS_CSG_COLUMN);
        taType.setCellValueFactory(new PropertyValueFactory<String, String>("type"));
        taType.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0/4.0));
        
        HBox taInfo = new HBox();
        ohInfo.getChildren().add(taInfo);
        csgBuilder.buildTextField(CSG_OH_TA_FIELD_NAME, taInfo, CLASS_CSG_FIELD, ENABLED);
        csgBuilder.buildTextField(CSG_OH_TA_FIELD_EMAIL, taInfo, CLASS_CSG_FIELD, ENABLED);
        csgBuilder.buildTextButton(CSG_OH_BUTTON_ADD_TA, taInfo, CLASS_CSG_BUTTON, ENABLED);
        
        HBox empty = new HBox();
        ohInfo.getChildren().add(empty);
        HBox ohHold = new HBox();
        ohInfo.getChildren().add(ohHold);
        csgBuilder.buildLabel(CSG_OH_LABEL_OH, ohHold, CLASS_CSG_LABEL, ENABLED);
        csgBuilder.buildLabel(CSG_OH_LABEL_START, ohHold, CLASS_CSG_LABEL, ENABLED);
        csgBuilder.buildComboBox(CSG_OH_OH_COMBO_START, startTime, "", ohHold, CLASS_CSG_COMBOBOX, ENABLED);
        csgBuilder.buildLabel(CSG_OH_LABEL_END, ohHold, CLASS_CSG_LABEL, ENABLED);
        csgBuilder.buildComboBox(CSG_OH_OH_COMBO_END, endTime, "", ohHold, CLASS_CSG_COMBOBOX, ENABLED);
        
        TableView<TimeSlot> ohTable = csgBuilder.buildTableView(CSG_OH_TABLEVIEW_OH, ohInfo, CLASS_CSG_TABLEVIEW, ENABLED);
        TableColumn start = csgBuilder.buildTableColumn(CSG_OH_OH_COLUMN_STARTTIME, ohTable, CLASS_CSG_COLUMN);
        start.setCellValueFactory(new PropertyValueFactory<String, String>("startTime"));
        start.prefWidthProperty().bind(ohTable.widthProperty().multiply(1.0/7.0));
        TableColumn end = csgBuilder.buildTableColumn(CSG_OH_OH_COLUMN_ENDTIME, ohTable, CLASS_CSG_COLUMN);
        end.setCellValueFactory(new PropertyValueFactory<String, String>("endTime"));
        end.prefWidthProperty().bind(ohTable.widthProperty().multiply(1.0/7.0));
        TableColumn monday = csgBuilder.buildTableColumn(CSG_OH_OH_COLUMN_MONDAY, ohTable, CLASS_CSG_COLUMN);
        monday.setCellValueFactory(new PropertyValueFactory<String, String>("monday"));
        monday.prefWidthProperty().bind(ohTable.widthProperty().multiply(1.0/7.0));
        TableColumn tuesday = csgBuilder.buildTableColumn(CSG_OH_OH_COLUMN_TUESDAY, ohTable, CLASS_CSG_COLUMN);
        tuesday.setCellValueFactory(new PropertyValueFactory<String, String>("tuesday"));
        tuesday.prefWidthProperty().bind(ohTable.widthProperty().multiply(1.0/7.0));
        TableColumn wednesday = csgBuilder.buildTableColumn(CSG_OH_OH_COLUMN_WEDNESDAY, ohTable, CLASS_CSG_COLUMN);
        wednesday.setCellValueFactory(new PropertyValueFactory<String, String>("wednesday"));
        wednesday.prefWidthProperty().bind(ohTable.widthProperty().multiply(1.0/7.0));
        TableColumn thursday = csgBuilder.buildTableColumn(CSG_OH_OH_COLUMN_THURSDAY, ohTable, CLASS_CSG_COLUMN);
        thursday.setCellValueFactory(new PropertyValueFactory<String, String>("thursday"));
        thursday.prefWidthProperty().bind(ohTable.widthProperty().multiply(1.0/7.0));
        TableColumn friday = csgBuilder.buildTableColumn(CSG_OH_OH_COLUMN_FRIDAY, ohTable, CLASS_CSG_COLUMN);
        friday.setCellValueFactory(new PropertyValueFactory<String, String>("friday"));
        friday.prefWidthProperty().bind(ohTable.widthProperty().multiply(1.0/7.0));

        //OFFICE HOURS TAB: ADD CONTENT
        oh.setContent(ohBox);
        ohTab.setContent(oh);
        tabPane.getTabs().add(ohTab);
        
        //SCHEDULE TAB
        ScrollPane sched = new ScrollPane();
        VBox schedBox = csgBuilder.buildVBox(CSG_SCHED_VBOX, null, CLASS_CSG_BACKGROUND, ENABLED);
        schedBox.prefWidthProperty().bind(tabPane.widthProperty().multiply(.99));
        schedBox.prefHeightProperty().bind(tabPane.heightProperty().multiply(1));
        
        VBox cal = csgBuilder.buildVBox(CSG_SCHED_VBOX_CALENDAR, schedBox, CLASS_CSG_BOX, ENABLED);
        csgBuilder.buildLabel(CSG_SCHED_LABEL_CALENDAR, cal, CLASS_CSG_BIGLABEL, ENABLED);
        HBox times = new HBox();
        cal.getChildren().add(times);
        csgBuilder.buildLabel(CSG_SCHED_LABEL_START_MON, times, CLASS_CSG_LABEL, ENABLED);
        csgBuilder.buildDatePicker(CSG_SCHED_DATEPICKER_START_MON, times, CLASS_CSG_DATEPICKER, ENABLED);
        csgBuilder.buildLabel(CSG_SCHED_LABEL_END_FRI, times, CLASS_CSG_LABEL, ENABLED);
        csgBuilder.buildDatePicker(CSG_SCHED_DATEPICKER_END_FRI, times, CLASS_CSG_DATEPICKER, ENABLED);
        
        VBox schedItem = csgBuilder.buildVBox(CSG_SCHED_VBOX_SCHED, schedBox, CLASS_CSG_BOX, ENABLED);
        HBox schedHold = new HBox();
        schedItem.getChildren().add(schedHold);
        csgBuilder.buildIconButton(CSG_SCHED_EXPAND_SCHED, schedHold, CLASS_CSG_MINIMIZE, ENABLED);
        csgBuilder.buildLabel(CSG_SCHED_LABEL_SCHED_ITEM, schedHold, CLASS_CSG_BIGLABEL, ENABLED);
        TableView schedTable = csgBuilder.buildTableView(CSG_SCHED_TABLEVIEW_SCHED, schedItem, CLASS_CSG_TABLEVIEW, ENABLED);
        TableColumn addType = csgBuilder.buildTableColumn(CSG_SCHED_COLUMN_TYPE, schedTable, CLASS_CSG_COLUMN);
        addType.setCellValueFactory(new PropertyValueFactory<String, String>("type"));
        addType.prefWidthProperty().bind(schedTable.widthProperty().multiply(1.0/7.0));
        TableColumn dateCol = csgBuilder.buildTableColumn(CSG_SCHED_COLUMN_DATE, schedTable, CLASS_CSG_COLUMN);
        dateCol.setCellValueFactory(new PropertyValueFactory<String, String>("date"));
        dateCol.prefWidthProperty().bind(schedTable.widthProperty().multiply(1.0/7.0));
        TableColumn titleCol = csgBuilder.buildTableColumn(CSG_SCHED_COLUMN_TITLE, schedTable, CLASS_CSG_COLUMN);
        titleCol.setCellValueFactory(new PropertyValueFactory<String, String>("title"));
        titleCol.prefWidthProperty().bind(schedTable.widthProperty().multiply(2.0/7.0));
        TableColumn topicCol = csgBuilder.buildTableColumn(CSG_SCHED_COLUMN_TOPIC, schedTable, CLASS_CSG_COLUMN);
        topicCol.setCellValueFactory(new PropertyValueFactory<String, String>("topic"));
        topicCol.prefWidthProperty().bind(schedTable.widthProperty().multiply(3.0/7.0));
        
        VBox addEdit = csgBuilder.buildVBox(CSG_SCHED_VBOX_ADD_EDIT, schedBox, CLASS_CSG_BOX, ENABLED);
        csgBuilder.buildLabel(CSG_SCHED_LABEL_ADD_EDIT, addEdit, CLASS_CSG_BIGLABEL, ENABLED);
        HBox typeHold = new HBox();
        addEdit.getChildren().add(typeHold);
        csgBuilder.buildLabel(CSG_SCHED_ADD_EDIT_LABEL_TYPE, typeHold, CLASS_CSG_LABEL, ENABLED);
        csgBuilder.buildComboBox(CSG_SCHED_ADD_EDIT_COMBO_TYPE, scheduleItemTypes, "", typeHold, CLASS_CSG_COMBOBOX, ENABLED);
        HBox dateHold = new HBox();
        addEdit.getChildren().add(dateHold);
        csgBuilder.buildLabel(CSG_SCHED_ADD_EDIT_LABEL_DATE, dateHold, CLASS_CSG_LABEL, ENABLED);
        csgBuilder.buildDatePicker(CSG_SCHED_ADD_EDIT_DATEPICKER_DATE, dateHold, CLASS_CSG_DATEPICKER, ENABLED);
        HBox titleHold = new HBox();
        addEdit.getChildren().add(titleHold);
        csgBuilder.buildLabel(CSG_SCHED_ADD_EDIT_LABEL_TITLE, titleHold, CLASS_CSG_LABEL, ENABLED);
        csgBuilder.buildTextField(CSG_SCHED_ADD_EDIT_FIELD_TITLE, titleHold, CLASS_CSG_FIELD, ENABLED);
        HBox topicHold = new HBox();
        addEdit.getChildren().add(topicHold);
        csgBuilder.buildLabel(CSG_SCHED_ADD_EDIT_LABEL_TOPIC, topicHold, CLASS_CSG_LABEL, ENABLED);
        csgBuilder.buildTextField(CSG_SCHED_ADD_EDIT_FIELD_TOPIC, topicHold, CLASS_CSG_FIELD, ENABLED);
        HBox linkHold = new HBox();
        addEdit.getChildren().add(linkHold);
        csgBuilder.buildLabel(CSG_SCHED_ADD_EDIT_LABEL_LINK, topicHold, CLASS_CSG_LABEL, ENABLED);
        csgBuilder.buildTextField(CSG_SCHED_ADD_EDIT_FIELD_LINK, topicHold, CLASS_CSG_FIELD, ENABLED);
        HBox buttonHold = new HBox();
        addEdit.getChildren().add(buttonHold);
        csgBuilder.buildTextButton(CSG_SCHED_ADD_EDIT_BUTTON_UPDATE, buttonHold, CLASS_CSG_BUTTON, ENABLED);
        csgBuilder.buildTextButton(CSG_SCHED_ADD_EDIT_BUTTON_CLEAR, buttonHold, CLASS_CSG_BUTTON, ENABLED);
        
        //SCHEDULE TAB: ADD CONTENT
        sched.setContent(schedBox);
        schedTab.setContent(sched);
        tabPane.getTabs().add(schedTab);
        
        main.getChildren().add(tabPane);
        workspace = new BorderPane();
        ((BorderPane) workspace).setCenter(tabPane);
    }
    
    public void initControllers() {
           AppGUIModule gui = app.getGUIModule();
           CourseSiteGenerator_Controller controller = new CourseSiteGenerator_Controller((CourseSiteGeneratorApp) app);
           Button btn_addTA = (Button) gui.getGUINode(CSG_OH_BUTTON_ADD_TA);
           Button btn_rmvTA = (Button) gui.getGUINode(CSG_OH_DELETE_TAS);
           RadioButton rbtn_all = (RadioButton) gui.getGUINode(CSG_OH_TA_RADIO_ALL);
           RadioButton rbtn_grad = (RadioButton) gui.getGUINode(CSG_OH_TA_RADIO_GRAD);
           RadioButton rbtn_under = (RadioButton) gui.getGUINode(CSG_OH_TA_RADIO_UNDERGRAD);
           TextField fld_name = (TextField) gui.getGUINode(CSG_OH_TA_FIELD_NAME);
           TextField fld_email = (TextField) gui.getGUINode(CSG_OH_TA_FIELD_EMAIL);
           TableView tbl_ta = (TableView) gui.getGUINode(CSG_OH_TABLEVIEW_TA);
           
           btn_addTA.setOnAction(e -> {
                controller.processAddTA(); 
           });
           
           fld_name.textProperty().addListener(e -> {
                controller.processTypeTA();
           });
           
           fld_email.textProperty().addListener(e -> {
                controller.processTypeTA();
           });
           
           btn_rmvTA.setOnAction(e -> {
              if (tbl_ta.getSelectionModel().getSelectedItem() == null) return;
              TeachingAssistant_Prototype ta = (TeachingAssistant_Prototype) tbl_ta.getSelectionModel().getSelectedItem();
              CourseSiteGenerator_Data data = (CourseSiteGenerator_Data) app.getDataComponent();
              RemoveTA_Transaction trans = new RemoveTA_Transaction(data, ta); 
              trans.doTransaction();
           });
           
           rbtn_all.selectedProperty().addListener(e -> {
               CourseSiteGenerator_Data data = (CourseSiteGenerator_Data) app.getDataComponent();
               controller.processTypeTA();
               data.switchTableViewType();
           });
           
           rbtn_grad.selectedProperty().addListener(e -> {
               CourseSiteGenerator_Data data = (CourseSiteGenerator_Data) app.getDataComponent();
               controller.processTypeTA();
               data.switchTableViewType();
           });
           
           rbtn_under.selectedProperty().addListener(e -> {
               CourseSiteGenerator_Data data = (CourseSiteGenerator_Data) app.getDataComponent();
               controller.processTypeTA();
               data.switchTableViewType();
           });
           
           tbl_ta.setOnMouseClicked(new EventHandler<MouseEvent>() {
               public void handle(MouseEvent event) {
                   CourseSiteGenerator_Data data = (CourseSiteGenerator_Data) app.getDataComponent();
                   if (data.isTASelected()) {
                       TeachingAssistant_Prototype ta = (TeachingAssistant_Prototype) tbl_ta.getSelectionModel().getSelectedItem();
                       controller.highlight(ta);
                   }
                   
                   if (event.getClickCount() == 2) {
                       editTAWindow.show();
                   }
               }
           });
         
        
           
        //Site Tab
        
        //TextFields and Text Area transaction
        TextField[] siteTextFields = new TextField[5];
        siteTextFields[0] = (TextField)gui.getGUINode(CSG_SITE_INSTRUCTOR_FIELD_NAME);
        siteTextFields[1] = (TextField)gui.getGUINode(CSG_SITE_INSTRUCTOR_FIELD_EMAIL);
        siteTextFields[2] = (TextField)gui.getGUINode(CSG_SITE_INSTRUCTOR_FIELD_ROOM);
        siteTextFields[3] = (TextField)gui.getGUINode(CSG_SITE_INSTRUCTOR_FIELD_HOME);
        siteTextFields[4] = (TextField)gui.getGUINode(CSG_SITE_BANNER_TEXTFIELD_TITLE);
        
        for (int i =0; i<siteTextFields.length; i++) {
            TextField textField = siteTextFields[i];
            textField.textProperty().addListener((observable,oldVal,newVal)->{
                CourseSiteGenerator_Data data = (CourseSiteGenerator_Data) app.getDataComponent();
                if (textField.isFocused()) {
                    Edit_Site_Transaction transaction = new Edit_Site_Transaction(data,(Node)textField, "TextField", oldVal, newVal);
                    app.processTransaction(transaction);
                }
        
            });
        }
        TextArea ohArea = (TextArea)gui.getGUINode(CSG_SITE_INSTRUCTOR_TEXTAREA_OH);
        ohArea.textProperty().addListener((observable,oldVal,newVal)->{
                CourseSiteGenerator_Data data = (CourseSiteGenerator_Data) app.getDataComponent();
                if (ohArea.isFocused()) {
                    Edit_Site_Transaction transaction = new Edit_Site_Transaction(data,(Node)ohArea, "TextArea", oldVal, newVal);
                    app.processTransaction(transaction);
                }
        
            });
        
        //CheckBox transactions 
        CheckBox[] siteCheckBoxs = new CheckBox[4];
        siteCheckBoxs[0] = (CheckBox)gui.getGUINode(CSG_SITE_PAGES_CHECK_HOME);
        siteCheckBoxs[1] = (CheckBox)gui.getGUINode(CSG_SITE_PAGES_CHECK_SYL);
        siteCheckBoxs[2] = (CheckBox)gui.getGUINode(CSG_SITE_PAGES_CHECK_SCHED);
        siteCheckBoxs[3] = (CheckBox)gui.getGUINode(CSG_SITE_PAGES_CHECK_HW);
        
         for (int i =0; i<siteCheckBoxs.length; i++) {
            CheckBox checkBox = siteCheckBoxs[i];
            checkBox.selectedProperty().addListener(e->{
                updateDir();
                CourseSiteGenerator_Data data = (CourseSiteGenerator_Data) app.getDataComponent();
                if (checkBox.isFocused()) {
                    gui.getGUINode(CSG_SITE_BANNER_LABEL_DIR).requestFocus(); //removes focus from checkbox
                    Edit_Site_Transaction transaction = new Edit_Site_Transaction(data,(Node)checkBox, "CheckBox", "", ""); //the old and new values don't matter, it is just flipped.
                    app.processTransaction(transaction);
                    
                }
        
            });        
         }
         ComboBox[] siteComboBoxes= new ComboBox[5];
         siteComboBoxes[0] = (ComboBox)gui.getGUINode(CSG_SITE_BANNER_COMBOBOX_SUBJECT);
         siteComboBoxes[1] = (ComboBox)gui.getGUINode(CSG_SITE_BANNER_COMBOBOX_NUMBER);
         siteComboBoxes[2] = (ComboBox)gui.getGUINode(CSG_SITE_BANNER_COMBOBOX_YEAR);
         siteComboBoxes[3] = (ComboBox)gui.getGUINode(CSG_SITE_BANNER_COMBOBOX_SEMESTER);
         siteComboBoxes[4] = (ComboBox)gui.getGUINode(CSG_SITE_STYLE_COMBOBOX_CSS);
         
        for (int i =0; i<siteComboBoxes.length; i++) {
            ComboBox comboBox = siteComboBoxes[i];
            if (comboBox==null) continue;
            comboBox.valueProperty().addListener((observable,oldVal,newVal)->{
                CourseSiteGenerator_Data data = (CourseSiteGenerator_Data) app.getDataComponent();
                updateDir();
                if (comboBox.isFocused()) {
                    gui.getGUINode(CSG_SITE_BANNER_LABEL_DIR).requestFocus(); //removes focus from checkbox
                    String oldString = null;
                    String newString = null;
                    if (oldVal!=null) oldString= oldVal.toString();
                    if (newVal!=null) newString= newVal.toString();
                    Edit_Site_Transaction transaction = new Edit_Site_Transaction(data,(Node)comboBox, "ComboBox", oldString, newString); //the old and new values don't matter, it is just flipped.
                    app.processTransaction(transaction);
                }
        
            });     
        }
        
        ((Button)gui.getGUINode(CSG_SITE_STYLE_ICON_FAV)).setOnAction(e->{  
           changeImage("FAV");
        });
        ((Button)gui.getGUINode(CSG_SITE_STYLE_ICON_NAV)).setOnAction(e->{
           changeImage("NAV");
        });
        ((Button)gui.getGUINode(CSG_SITE_STYLE_ICON_LEFT)).setOnAction(e->{
           changeImage("LEFT");
        });
        ((Button)gui.getGUINode(CSG_SITE_STYLE_ICON_RIGHT)).setOnAction(e->{
           changeImage("RIGHT");
        });


        
        //Syllabus Page Transactions
         TextArea[] sylArea= new TextArea[9];
         sylArea[0] = (TextArea)gui.getGUINode(CSG_SYL_TEXTAREA_DESCRIPTION);
         sylArea[1] = (TextArea)gui.getGUINode(CSG_SYL_TEXTAREA_TOPICS);
         sylArea[2] = (TextArea)gui.getGUINode(CSG_SYL_TEXTAREA_PREREQ);
         sylArea[3] = (TextArea)gui.getGUINode(CSG_SYL_TEXTAREA_OUTCOMES);
         sylArea[4] = (TextArea)gui.getGUINode(CSG_SYL_TEXTAREA_TEXTBOOKS);
         sylArea[5] = (TextArea)gui.getGUINode(CSG_SYL_TEXTAREA_GRADED);
         sylArea[6] = (TextArea)gui.getGUINode(CSG_SYL_TEXTAREA_NOTE);
         sylArea[7] = (TextArea)gui.getGUINode(CSG_SYL_TEXTAREA_DISHONESTY);
         sylArea[8] = (TextArea)gui.getGUINode(CSG_SYL_TEXTAREA_ASSIST);
         
        for (int i=0; i<sylArea.length; i++) {
            TextArea area = sylArea[i];
            area.textProperty().addListener((observable,oldVal,newVal)->{
               if(area.isFocused()) {
                    CourseSiteGenerator_Data data = (CourseSiteGenerator_Data) app.getDataComponent();
                    Edit_Syllabus_Transaction transaction = new Edit_Syllabus_Transaction(data,area,oldVal,newVal);
                    app.processTransaction(transaction);
               }
            });
        }
        
        //Meeting Time Page Transactions
        ((Button)gui.getGUINode(CSG_MEET_ADD_LECTURES)).setOnAction(e-> {
            CourseSiteGenerator_Data data = (CourseSiteGenerator_Data)app.getDataComponent();
            app.processTransaction(new Add_Remove_Meeting_Transaction(data,new MeetingTime("Lecture"),"lec", false));
        });
        ((Button)gui.getGUINode(CSG_MEET_ADD_RECITATIONS)).setOnAction(e-> {
            CourseSiteGenerator_Data data = (CourseSiteGenerator_Data)app.getDataComponent();
            app.processTransaction(new Add_Remove_Meeting_Transaction(data,new MeetingTime("Recitation"),"rec", false));
        });
        ((Button)gui.getGUINode(CSG_MEET_ADD_LABS)).setOnAction(e-> {
            CourseSiteGenerator_Data data = (CourseSiteGenerator_Data)app.getDataComponent();
            app.processTransaction(new Add_Remove_Meeting_Transaction(data,new MeetingTime("Lab"),"lab", false));
        });
        //Remove
        ((Button)gui.getGUINode(CSG_MEET_REMOVE_LECTURES)).setOnAction(e-> {
            TableView table = (TableView)gui.getGUINode(CSG_MEET_TABLEVIEW_LECTURES);
            if(table.getSelectionModel().getSelectedItem()==null) return;
            CourseSiteGenerator_Data data = (CourseSiteGenerator_Data)app.getDataComponent();
            app.processTransaction(new Add_Remove_Meeting_Transaction(data,(MeetingTime)table.getSelectionModel().getSelectedItem(),"lec", true));
        });
        ((Button)gui.getGUINode(CSG_MEET_REMOVE_RECITATIONS)).setOnAction(e-> {
            TableView table = (TableView)gui.getGUINode(CSG_MEET_TABLEVIEW_RECITATIONS);
            if(table.getSelectionModel().getSelectedItem()==null) return;
            CourseSiteGenerator_Data data = (CourseSiteGenerator_Data)app.getDataComponent();
            app.processTransaction(new Add_Remove_Meeting_Transaction(data,(MeetingTime)table.getSelectionModel().getSelectedItem(),"rec", true));
        });
        ((Button)gui.getGUINode(CSG_MEET_REMOVE_LABS)).setOnAction(e-> {
            TableView table = (TableView)gui.getGUINode(CSG_MEET_TABLEVIEW_LABS);
            if(table.getSelectionModel().getSelectedItem()==null) return;
            CourseSiteGenerator_Data data = (CourseSiteGenerator_Data)app.getDataComponent();
            app.processTransaction(new Add_Remove_Meeting_Transaction(data,(MeetingTime)table.getSelectionModel().getSelectedItem(),"lab", true));
        });
        
        
        //Cell Edit
        TableView<MeetingTime> meet;
        ObservableList<TableColumn<MeetingTime,?>> columns;
        
        meet=((TableView<MeetingTime>)gui.getGUINode(CSG_MEET_TABLEVIEW_LECTURES));
        columns = meet.getColumns();
        for (int j=0; j<columns.size(); j++) {
               columns.get(j).setOnEditCommit(e -> {
                   app.processTransaction(new Edit_Meeting_Transaction(e.getOldValue().toString(),e.getNewValue().toString(), e.getTablePosition().getRow(),e.getTableColumn()));
            });
        }
        meet=((TableView<MeetingTime>)gui.getGUINode(CSG_MEET_TABLEVIEW_RECITATIONS));
        columns = meet.getColumns();
        for (int j=0; j<columns.size(); j++) {
               columns.get(j).setOnEditCommit(e -> {
                   app.processTransaction(new Edit_Meeting_Transaction(e.getOldValue().toString(),e.getNewValue().toString(), e.getTablePosition().getRow(),e.getTableColumn()));
            });
        }
        meet=((TableView<MeetingTime>)gui.getGUINode(CSG_MEET_TABLEVIEW_LABS));
        columns = meet.getColumns();
        for (int j=0; j<columns.size(); j++) {
               columns.get(j).setOnEditCommit(e -> {
                   app.processTransaction(new Edit_Meeting_Transaction(e.getOldValue().toString(),e.getNewValue().toString(), e.getTablePosition().getRow(),e.getTableColumn()));
            });
        }
        
        //Schedule Tab
        ((Button)gui.getGUINode(CSG_SCHED_ADD_EDIT_BUTTON_UPDATE)).setOnAction(e->{
            TableView table = (TableView)gui.getGUINode(CSG_SCHED_TABLEVIEW_SCHED);
                CourseSiteGenerator_Data data = (CourseSiteGenerator_Data)app.getDataComponent();
                String type = "";
                
                    type = ((ComboBox)gui.getGUINode(CSG_SCHED_ADD_EDIT_COMBO_TYPE)).getSelectionModel().getSelectedItem().toString();
                    DatePicker date = (DatePicker)gui.getGUINode(CSG_SCHED_ADD_EDIT_DATEPICKER_DATE);
                    String month = "";
                    if(date.getEditor().getText().length()>0) {
                        month = date.getEditor().getText();
                        if (month.indexOf("/")>=0) month = month.substring(month.indexOf("/"));
                    }
                    if (date.getValue()!=null) month= ""+date.getValue().getMonthValue();
                     String day = "";
                      if(date.getEditor().getText().length()>0) {
                        day = date.getEditor().getText();
                        if (day.indexOf("/")>=0 && day.lastIndexOf("/")!=day.indexOf("/")) day = day.substring(day.indexOf("/")+1,day.lastIndexOf("/"));
                      }
                    if (date.getValue()!=null) day= ""+date.getValue().getDayOfMonth();
                        String title = ((TextField)gui.getGUINode(CSG_SCHED_ADD_EDIT_FIELD_TITLE)).getText();
                        String topic = ((TextField)gui.getGUINode(CSG_SCHED_ADD_EDIT_FIELD_TOPIC)).getText();
                        String link = ((TextField)gui.getGUINode(CSG_SCHED_ADD_EDIT_FIELD_LINK)).getText();
                        if (!type.equals("") && !month.equals("") && !day.equals("") && !title.equals("") &&!topic.equals("") && !link.equals("")) {
                            
                            //When editing
                            if (table.getSelectionModel().getSelectedItem()!=null) {
                                ScheduleItem scheduleItem = (ScheduleItem)table.getSelectionModel().getSelectedItem();
                                ScheduleItem newItem = new ScheduleItem(type,month,day,title,topic,link);
                                app.processTransaction(new Edit_Schedule_Transaction(data,scheduleItem,newItem));
                                
                            }
                            //when creating a new item
                            else {
                            ScheduleItem scheduleItem = new ScheduleItem(type,month,day,title,topic,link);
                            app.processTransaction(new Add_Remove_Schedule_Transaction(data,scheduleItem,false));
                            }
                        }
        });
        ((TableView)gui.getGUINode(CSG_SCHED_TABLEVIEW_SCHED)).setOnMouseClicked(e->{
                    TableView table =(TableView)gui.getGUINode(CSG_SCHED_TABLEVIEW_SCHED);
                    if(table.getSelectionModel().getSelectedItem()==null) return;
                     ScheduleItem scheduleItem = (ScheduleItem)table.getSelectionModel().getSelectedItem();
                    ((TextField)gui.getGUINode(CSG_SCHED_ADD_EDIT_FIELD_TITLE)).setText(scheduleItem.getTitle());
                    ((TextField)gui.getGUINode(CSG_SCHED_ADD_EDIT_FIELD_TOPIC)).setText(scheduleItem.getTopic());
                    ((TextField)gui.getGUINode(CSG_SCHED_ADD_EDIT_FIELD_LINK)).setText(scheduleItem.getLink());
                    ((DatePicker)gui.getGUINode(CSG_SCHED_ADD_EDIT_DATEPICKER_DATE)).getEditor().setText(scheduleItem.getDate());
                    ((ComboBox)gui.getGUINode(CSG_SCHED_ADD_EDIT_COMBO_TYPE)).getSelectionModel().select(scheduleItem.getType());
        
        });
        ((Button)gui.getGUINode(CSG_SCHED_EXPAND_SCHED)).setOnAction(e->{
            TableView table = (TableView)gui.getGUINode(CSG_SCHED_TABLEVIEW_SCHED);
            if(table.getSelectionModel().getSelectedItem()!=null) {
                ScheduleItem scheduleItem = (ScheduleItem)table.getSelectionModel().getSelectedItem();
                CourseSiteGenerator_Data data = (CourseSiteGenerator_Data)app.getDataComponent();
                app.processTransaction(new Add_Remove_Schedule_Transaction(data,scheduleItem,true));
            }
        });
        ((Button)gui.getGUINode(CSG_SCHED_ADD_EDIT_BUTTON_CLEAR)).setOnAction(e->{
            CourseSiteGenerator_Data data = (CourseSiteGenerator_Data)app.getDataComponent();
            app.processTransaction(new Clear_Schedule_Transaction(data));
        
        });

        
        //update export dir automatically on language change
        ((Label)gui.getGUINode(CSG_SITE_BANNER_LABEL_DIR_ERROR)).textProperty().addListener(e->{
            updateDir();
        });
        
         
           

    }
    private void loadCSSFiles() {
        css.clear();
        File folder = new File("work\\css\\");
        File[] filesList = folder.listFiles();
        
        for (File cssFile: filesList) {
            if (cssFile.isFile()) {
                css.add(cssFile.getName());
            }
        }
    }
    
    
    private void updateDir() {
        AppGUIModule gui = app.getGUIModule();
        if(((ComboBox)gui.getGUINode(CSG_SITE_BANNER_COMBOBOX_SUBJECT)).getSelectionModel().getSelectedIndex()==0) return;
        if(((ComboBox)gui.getGUINode(CSG_SITE_BANNER_COMBOBOX_YEAR)).getSelectionModel().getSelectedIndex()==0) return;
        if(((ComboBox)gui.getGUINode(CSG_SITE_BANNER_COMBOBOX_SEMESTER)).getSelectionModel().getSelectedIndex()==0) return;
        if(((ComboBox)gui.getGUINode(CSG_SITE_BANNER_COMBOBOX_NUMBER)).getSelectionModel().getSelectedIndex()==0) return;
        String subjectString = ((ComboBox)gui.getGUINode(CSG_SITE_BANNER_COMBOBOX_SUBJECT)).getEditor().getText();
        if (!subject.contains(subjectString) && !subjectString.isEmpty()) { //add any unique subjects
            subject.add(subjectString);
        }
        String numberString = ((ComboBox)gui.getGUINode(CSG_SITE_BANNER_COMBOBOX_NUMBER)).getEditor().getText();
        if (!number.contains(numberString) && !numberString.isEmpty()) { //add any unique numbers
            number.add(numberString);
        }
        
        String semesterString = ((ComboBox)gui.getGUINode(CSG_SITE_BANNER_COMBOBOX_SEMESTER)).getEditor().getText();
        String yearString = ((ComboBox)gui.getGUINode(CSG_SITE_BANNER_COMBOBOX_YEAR)).getEditor().getText();
        
        boolean shouldExport = ((CheckBox)gui.getGUINode(CSG_SITE_PAGES_CHECK_HOME)).isSelected();
        shouldExport |= ((CheckBox)gui.getGUINode(CSG_SITE_PAGES_CHECK_SYL)).isSelected();
        shouldExport |= ((CheckBox)gui.getGUINode(CSG_SITE_PAGES_CHECK_SCHED)).isSelected();
        shouldExport |= ((CheckBox)gui.getGUINode(CSG_SITE_PAGES_CHECK_HW)).isSelected();
        
        shouldExport &= (!subjectString.isEmpty() && !semesterString.isEmpty() && !numberString.isEmpty() && !yearString.isEmpty()); //if any are empty, do not export.
        String exportText = ".\\export\\"+subjectString+"_"+numberString+"_"+semesterString+"_"+yearString+"\\public.html";
        if (shouldExport==true) {
            ((Label)gui.getGUINode(CSG_SITE_BANNER_LABEL_DIR_EDIT)).setText(exportText);
            exportDirectory = subjectString+"_"+numberString+"_"+semesterString+"_"+yearString;
        }
        else {
            exportText = ((Label)gui.getGUINode(CSG_SITE_BANNER_LABEL_DIR_ERROR)).getText();
            ((Label)gui.getGUINode(CSG_SITE_BANNER_LABEL_DIR_EDIT)).setText(exportText);
            exportDirectory = exportText;
        }
        
        
    
    }

    public void initFoolproofDesign() {
        AppGUIModule gui = app.getGUIModule();
        AppFoolproofModule foolproofSettings = app.getFoolproofModule();
        foolproofSettings.registerModeSettings(CSG_FOOLPROOF_SETTINGS,
                new CourseSiteGenerator_FoolproofDesign((CourseSiteGeneratorApp) app));
    }
    
    @Override
    public void showNewDialog() {
       //NOT IN APPLICATION
    }

    @Override
    public void processWorkspaceKeyEvent(KeyEvent ke) {
        //NOT NEEDED IN APPPLICATION
    }
    public void changeImage(String imageType) {
       AppGUIModule gui = app.getGUIModule();
       AppNodesBuilder csgBuilder = gui.getNodesBuilder();
       CourseSiteGenerator_Data data = (CourseSiteGenerator_Data)app.getDataComponent();
       
      fileChooser.getExtensionFilters().addAll(
           new FileChooser.ExtensionFilter("All Images", "*.*"),
           new FileChooser.ExtensionFilter("JPG", "*.jpg"),
           new FileChooser.ExtensionFilter("PNG", "*.png")
      );
      File file = fileChooser.showOpenDialog(gui.getWindow());
      if (file != null) {
         String path = file.getPath();
         String imageViewId = "CSG_SITE_STYLE_ICON_"+imageType;
         
         String oldImage = "";
         if(imageType.equals("FAV")) {oldImage = favicon;favicon = file.getName();}
         else if(imageType.equals("NAV")) {oldImage = navbar; navbar = file.getName();}
         else if(imageType.equals("LEFT")){ oldImage = leftFooter; leftFooter = file.getName();}
         else {oldImage = rightFooter; rightFooter = file.getName();}
         
         Image im = new Image("file:"+path) {};
         saveImage(im,file.getName());
         
         Edit_Site_Transaction trans = new Edit_Site_Transaction(data,(Button)gui.getGUINode(imageViewId), "image",oldImage,file.getName());
         app.processTransaction(trans);
         
      }
      
   }
   public static void saveImage(Image image, String name) {
    File outputFile = new File("images\\"+name);
    BufferedImage bImage = SwingFXUtils.fromFXImage(image, null);
    try {
      ImageIO.write(bImage, "png", outputFile);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  } 
   public void setImages() {
       AppGUIModule gui= app.getGUIModule();
        String list [] = {"FAV","NAV","LEFT","RIGHT"};
        for (int i=0; i<list.length; i++) {
         String name = list[i];
         String value = "";
         if(i==0) value = favicon;
         else if(i==1) value = navbar;
         else if(i==2) value = leftFooter;
         else if(i==3) value = rightFooter;
         if (value.length()==0) continue;
         Button b = (Button)gui.getGUINode("CSG_SITE_STYLE_ICON_"+name);
         File file = new File("images\\"+value);   
         if (file != null) {
         Image im = new Image("file:"+file.getPath());
         ImageView imageView = (ImageView)b.getGraphic();
         if (imageView==null)imageView = new ImageView();
         imageView.setImage(im);
         if (im==null) continue;
         imageView.setFitHeight(40);
         imageView.setFitWidth(100);
         b.setGraphic(imageView);
        }
       }
      
   }
    public void reset() {
          favicon = "";
          navbar = "";
          leftFooter = "";
          rightFooter = "";
          exportDirectory = "";
      }



}
